

This is a `node.js` component that wraps a simple `http` API around `i18-next` to expose translations to backend components that run in foreign language runtimes. 

It is intended to be deployed as a _sidecar_  and to be accessed through the loopback network interface.

This is a standalone component, to be used in the context of multiple `apprise` application. In particular, translation files are external resources and will be mounted at deployment time with any mechanism available in the target platform. 

## Build
--------

It's a `node.js` application:

- written `typescript`,
- linted with `eslint`,
- transpiled by `esbuild`,
- into `commonjs` modules.

Managed with `yarn`, suppports:

- `yarn start`:  lints, typechecks, transpiles, and `serve`s the app, restarting it on changes to code or build scripts.
- `yarn ci`:  lints strictly, typechecks, transpiles, and runs the app.

Run `yarn start` in development and `yarn build` in CI.

Some details:

* watching realies on `nodemon`, with configuration in `nodemon.js`. 
* `nodemon` watches all build scripts  as well as the source code, including `nodemon.js` itself. 
* the linter configuration is in `.eslintrc.js`.
* the type checking configuration is in `tsconfig.json`.
* the `esbuild` configuration is in `esbuild.js`, and uses automatically settings from `tsconfig.json`. 
* the build is bundled, so that it can be started without installing dependencies. 
* the build includes links to external sourcemaps, limited to application sources (excludes bundlàùed dependencies).

## Debugging
-----------

`yarn start` opens a port (`9229`) for debugging.  In `VS Code`, this launch configuration in `.vscode/launch.json` attaches the debugger to the `node` process:

```json
{ 
    "projectName": "...",
    "name": "Attach Node",
    "request": "attach",
    "type": "node",
    "hostName": "localhost",
    "port": 9229
}
```

Alternatively, this launch configuration runs the app in the debugger (`F5`):

```json
{
        
    "projectName": "...",
    "name": "Debug Node",
    "request": "launch",
    "type": "node",
    "runtimeArgs": [ "build/index.js"]

```

## Externals
------------

Packages that cannot be bundled can be specifies as `dependencies` in `externals.json`. 

This is a minimal module manifest and can be installed to run the code with minimal dependencies.

## Resources
-------------

During development, mount test translations under `resources/locale`, and they will be copied into the build output.
The wlll not be under source control.


## Image
--------

There's a `Dockerfile`  to packages the application in a `Docker` image. In particular, it:

* copies `externals.json` as `package.json` under a root `app` folder.
* runs `yarn install` to install the external dependencies.
* copies the build output to the app folder.
* finally, lunches node with the transpiled code.

The image should be created after a build process, typically `yarn ci`. Dependency installation should be fast, as there should be very few externals and the image layers will be reused as long as there are no changes to `externals.json`. Running containers should also be snappy, as there are not further installation costs.



