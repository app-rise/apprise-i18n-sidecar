FROM node:17-alpine

WORKDIR /i18n/
ENV NODE_ENV production
ENV NPM_CONFIG_LOGLEVEL warn

COPY externals.json ./package.json

RUN  yarn install --frozen-lockfile && yarn cache clean --mirror

COPY build ./

CMD node index.js