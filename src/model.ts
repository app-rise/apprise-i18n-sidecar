import { TFunction, TOptions } from 'i18next';
import { defaultLanguage, defaultSupportedLanguages } from './constants';

export type Language = 'ar' | 'en' | 'es' | 'fr' | 'ru' | 'zh'


export type TArgs = {

    key: string

    params?: TParams
    options?: TOptions

}

export type TParams = Record<string, Multilang | TArgs | string>

// capture 
export type TCall = TArgs & Partial<{

    lng: Language
    template: boolean
}>


export type Multilang = Partial<Record<Language,string>>

export const localise = (lng:Language) => (ml: Multilang | string | undefined | null = {}) => (isMultilang(ml) ? ( ml[lng] ?? ml[defaultLanguage] ) : ml) as string

export const isMultilang = (ml: any) : ml is Multilang => !!ml && typeof ml === 'object' && Object.keys(ml).every(k=> defaultSupportedLanguages.includes(k as Language)) 

export type TemplateRenderer = ( t: TFunction, params: TParams, originalParams: TParams) => Promise<string>