import i18next from 'i18next';
import Backend from "i18next-fs-backend";
import { resolve } from 'path';
import { defaultLanguage, localePath, supportedLanguages } from './constants';
import { isMultilang, Language, localise, Multilang, TemplateRenderer, TParams } from './model';

export const initIntl = () =>

    i18next.use(Backend).init({
        preload: supportedLanguages,
        fallbackLng: defaultLanguage,
        backend: {
            loadPath: () => resolve(__dirname, localePath)
        }

    })



export const translate = async (renderer: TemplateRenderer, params: TParams, lng?:Language ) : Promise<Multilang | string> => {

    const render = async (lng: Language) =>  {

        const t = i18next.getFixedT(lng)

        // trasnlate params using embedded multilang or references to other keys.
        const translateParams = (params:TParams) : TParams => Object.entries(params).reduce((acc, [name, val]) => {

            if ( typeof val === 'string')
                return ({ ...acc, [name]:val })

            const { key, params = {}, options = {} } = isMultilang(val) ? { key: localise(lng)(val) } : val

            return ({ ...acc, [name]: t(key, {...options, ...translateParams(params), nsSeparator: false} ) })

        }, {
            lng
        })

        const translatedParams = translateParams(params)

        console.log({translatedParams})
        
        // pass function and params to renderer, if one is provided. otherwise translate key directly. 
        return await renderer(t, translatedParams, params)

    }

    if (lng)
        return render(lng)

    const translations = await Promise.all( supportedLanguages.map( lng => render(lng).then( translation => [lng,translation] as const) ) )

    return translations.reduce( (acc, [lng,translation]) =>  ({ ...acc, [lng]: translation }) , {} as Multilang)


}