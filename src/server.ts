

import fastify from 'fastify'
import { port } from './constants'
import traps from '@dnlup/fastify-traps'

const server = fastify()



export const initServer = () => {

    server.register(traps)

    server.listen(port, '0.0.0.0', (err) => {  // listens to all addresses in-container

        if (err)
            throw err

        console.log(`ready listening on ${port}...`)

    })

    return server
}