import { Language } from './model'


export const defaultPort = 9999

export  const port = process.env.PORT ?? defaultPort

export const defaultSupportedLanguages: Language[] = ['en', 'ar', 'es', 'fr', 'ru', 'zh']
export  const supportedLanguages = process.env.APP_LANGS?.split(',') as Language[] ??  defaultSupportedLanguages

export const defaultDefaultLanguage = 'en'
export  const defaultLanguage = process.env.APP_FALLBACK_LANG as Language ??  supportedLanguages[0]

export const localePath = 'locale/{{lng}}_translation.json'
export const templatePath = 'template'

export const translateApi="/"