
import { FastifyInstance } from 'fastify';
import { templatePath, translateApi } from './constants';
import { translate } from './intl';
import { TCall, TemplateRenderer } from './model';

const rendererFor = async (call: TCall): Promise<TemplateRenderer> => {

    const { template, key, options } = call

    const defaultRenderer : TemplateRenderer = (t, params) => t(key, { ...options, ...params })

    const module = template ? await import(`./${templatePath}/${key}.mjs`) : { render : defaultRenderer}

    return module.render
}

export const initApi = (server: FastifyInstance) => server


    .post<{ Body: TCall }>(`${translateApi}`,

        async ({ body }, reply) => {

            if (!body)
                return reply.code(400).send(new Error('malformed request: no input.'))

            const { key, params = {}, lng } = body

            if (!key)
                return reply.code(400).send(new Error('malformed request: no key.'))

            try {

                const renderer = await rendererFor(body)

                const rendered = await translate( renderer, params, lng)

                return rendered

            }
            catch (e) {
                console.warn(e)
                return reply.code(400).send(new Error(`can't process ${key} as a template: ${e}.`))
            }

        })


    .get<{ Params: { key: string }, Querystring: Pick<TCall,'lng'|'template'>}>(`${translateApi}:key`,

        async (request, reply) => {

            const key = request.params.key

            if (!key)
                return reply.code(400).send(new Error('malformed request: no key.'))

            const call = { key, template: !!request.query.template, lng: request.query.lng }

            const renderer = await rendererFor(call)

            const rendered = await translate(renderer, {} ,call.lng )

            return rendered

        })

