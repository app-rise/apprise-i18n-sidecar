
import { initApi } from './api'
import { defaultLanguage, supportedLanguages } from './constants'
import { initIntl } from './intl'
import { initServer } from './server'



initIntl().then(initServer).then(initApi).finally(()=>{

    console.log("languages:", `configured: ${process.env.APP_LANGS}`, `default: ${supportedLanguages}`)
    console.log("fallback:", `configured: ${process.env.APP_FALLBACK_LANG}`, `default: ${defaultLanguage}`)
})



